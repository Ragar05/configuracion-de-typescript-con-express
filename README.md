# Desarrollo en Typescript con Express

Durante el desarollo de su aplicacion express, en typescript, puede tener una serie de incovenientes que pueden ser desafiantes hasta el punto de causar un gran estres por la poca documentacion en español

>NOTA: Para este punto de debe conocer como inicilizar un proyecto typescript, instalar dependencias de desarrollo y lo
>basico en la configuracion de un archivo tsconfig.json

## Uso de ts-node

Si esta usando ts-node para ejecutar su codigo typescript debe saber, que ***ts-node*** no reconoce muchos de las funcionalidades que le ofrece typescript. por lo tanto se le ofrecera una seria de conejos para que no presente problemas a la hora de ejecutar su codigo en la etapa de desarrollo.

### Configuracion de nodemon

Para que usted pueda recargar su codigo typescript en el instante en que este reciba un cambio. nodemon es una herramienta muy util ya que le ofrece configurarlo a su gusto. Por lo tanto antes de proseguir debe instalarlo como dependencia de desarrollo.

```json
//nodemon.json
{
    "watch": ["src"], 
    "ext": "ts,json",
    "exec": "ts-node ./src/app.ts"
}
```

* Propiedades
  * **watch**  Se le indica en un arreglo, las carpetas a las cuales nodemon estara observando.
  * **ext** Se escriben las extensiones en las cuales nodemon estara pendiente, para recargar la aplicacion en caso de que alguno de los archivos reciba un cambio.
  * **exec** Se configura el comando con el cual se ejecutara nodemon cuando este se llame por defecto. En este caso se le dice que ejecute el proyecto usando ts-node con el archivo ***app.ts*** que esta en la carpeta ***src***.

>**NOTA**: debe crear el archivo nodemon.json en el directorio raiz de su proyecto

***Verifique que su package.json tenga estos paquetes en sus devDependencies***  
  
```json
"devDependencies": {
    "@types/node": "^17.0.31",
    "nodemon": "^2.0.16",
    "ts-node": "^10.7.0",
    "typescript": "^4.6.4"
},
```

>**NOTA**: si ve paquetes que no conoce, como typescript o @types/node, le recomiendo investigar sobre como inicializar un proyecto typescript

***Ejecute el siguiente comando, y si siguio todo al pie de la letra. Debera de ejecutarse su codigo con total satisfaccion.***

```bash
    nodemon
```

> **NOTA**: ***Si recibe un error***, es probable que su sistema no reconozca lo que su proyecto tenga instalado, por lo tanto, es recomendable instalar nodemon de forma global en el sistema o, en su preferencia puede construir un script en su package.json.

***Construccion de un script para nodemon***

```json
{
   "scripts": {
    "dev": "nodemon"
  },
}
```

----

## Expansion de los propiedades para los tipos de Express

Antes de seguir, debe de haber instalado en su proyecto los paquetes ***express*** como dependencia y ***@types/express*** como dependencia de desarrollo.

### Expansion de propiedades

Typescript, al ser un lenguaje de tipado estatico, las librerias que se trabajan directamente con javascript, tienen sus modulos de tipos a parte. Por lo tanto estos tipos por si traen valores predefinidos. que pueden ser modifcables hasta cierto punto, ya sea a traves de ***interfaces*** o ***types*** depediento de la estructura con la que se construya.

***Configuracion Previa***

Antes de seguir con este punto, si esta usando ts-node le sera de mucha ayuda ya que se va a aclarar un punto con respecto a los archivos de declaraciones locales. Y es que ts-node no reconoce este tipo de archivos, por lo tanto puede que en su sintaxis su codigo se vea impecable, pero al momento de ejecutar la aplicacion con ***ts-node*** o directamente transpilando el codigo a javascript con ***tsc*** puede que salte un error.

```ts
//example.controller.ts
import express from "express";

const controllerExample = (request:express.Request, response:express.Response) => {
    const info =  request.info;

    if(!info){
        return response.status(400).json({
            ok:false,
            message: "no existe la propiedad info"
        })
    }
    
    return response.status(200).json({
        ok:true,
        message: "respuesta valida"
    })
}
```

Como se puede evidenciar en el codigo de ejemplo, cuando se llege al ***request.info***, ts-node o el transpilador de typescript, arrojara un error, señalando que la propiedad ***info*** del objeto ***request***, no este definida en el type ***express.Request***. La solucion a este problema radica en crear un archivo de declaracion local.

***Expandiendo el tipo Request***

En la raiz del proyecto, se crea un archivo, ***index.d.ts***. Aunque la ubicacion es de su preferencia, en nuestro caso se creara dentro de una carpeta ***@types*** que estara dentro del proyecto de la carpeta que fue definida en la propiedad ***rootDir*** del archivo ***tscondfig.json***

```ts
//./src/@types/index.d.ts

export {};

declare global {
  namespace Express {
     interface Request {
      info: string;
    }
  }
}

```

Se declara de forma global que la interface Request, tendra un una propiedad mas, que es **info** y es de tipo ***string***. Este ejemplo puede ser aplicado a cualquier caso, puede que la propiedad info, sea un type personalizado, aunque eso ya depende de las necesidades que tenga.
  
***Configurando el archivo tsconfig***

En caso de que haya declarado su archivo fuera del *rootDir* donde se encuentra su codigo typescript, es decir, en el directorio raiz del proyecto, le recomiendo realizar la siguiente configuracion en su ***tsconfig.json***.

```json
{
    "compilerOptions":{
        "typeRoots": ["./src/@types"]
        /// aqui debe de estar su configuracion del proyecto
    }
}
```

***Configuracion si usa ts-node***

En caso de que use ts-node, y al ejecutar la aplicacion nota que ts-node le arroja un error, esto no quiere decir, que haya hecho las cosas mal. Lo que pasa aqui y como se aclare anterior mente en un principio, ts-node no reconoce los archivos de declaraciones locales. por lo tanto debe agregarle un flag y volver a ejecutar la aplicacion.

* ***Configurando nodemon.json***

 ```json
 {
    "watch": ["src"], 
    "ext": "ts,json",
    "exec": "ts-node --files ./src/app.ts"
 }
 ```

El flag usado es ***--files***, el cual incluye todos archvivos declarado en el proyecto. Un punto que explica esto es que ts-node no usa *files*, *include* o *exclude*. Esto se debe a que la mayoria de proyectos no usan todos los archivos de un directorio de proyecto.

----

## Tipar la propiedad body, params o query del type Request

Por defecto, el type Request, de express ofrece tipar el body, params o incluso los querys, pero puede ser muy engorroso. La manera en la que se puede realizar esto de forma estructurada y ordenada es creando, un directorio de ***types*** dentro del directorio declarado en el ***rootDir***. Usaremos los tipos genericos en esta solucion.

```ts
// ./src/types/index.ts
import {Request, ParamsDictionary, Query} from 'express-serve-static-core'

//Tipando la propiedad body del objeto request
export interface IRequestBody<T> extends Request {
    body: T
}

//Tipando la propiedad params del objeto request
export interface IRequestParams<T extends ParamsDictionary> extends Request {
    params: T
}

//Tipando la propiedad query del objeto request
export interface IRequestQuery<T extends Query> extends Request {
    query: T
}
```

Se seguira expandiendo esta documentacion, mientras vayan surgiendo problemas los cuales pueda resolver
